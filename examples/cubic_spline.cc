#include <iostream>
#include <fstream>
#include <vector>
#include "src/spline.h"

using namespace std;

int main(int argc, char *argv[])
{
  vector<double> X, Y;
  
  ifstream ifs(argv[1]);
  double x, y;
  while ( ifs >> x >> y ) {
    X.emplace_back(x);
    Y.emplace_back(y);
  }
  ifs.close();

  const double max_x = *std::max_element(X.begin(), X.end());
  const double min_x = *std::min_element(X.begin(), X.end());
  
  green::tk::spline s(X, Y);

  const int N = 200;
  const double dx = (max_x-min_x)/(N-1);
  for (int i = 0; i < N; ++i) {
    double x = min_x+i*dx;
    cout << x << " " << s(x) << endl;
  }
  
  return 0;
}
