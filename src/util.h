#ifndef UTIL_H
#define UTIL_H

#include <Eigen/Sparse>
#include "types.h"

namespace green {

extern "C" {
  void ld_by_order_(int *order, double *x, double *y, double *z, double *w);
  void xyz_to_tp_(double *x, double *y, double *z, double *phi, double *theta);
}

inline bool isAtOrigin(const double square_r) {
  return square_r < 1e-18;
}

inline void xyz_to_pt(const double &&x, const double &&y, const double &&z,
                             double *phi, double *theta) {
  xyz_to_tp_(const_cast<double*>(&x),
             const_cast<double*>(&y),
             const_cast<double*>(&z),
             phi, theta);
}

inline void xyz_to_rpt(const double &x, const double &y, const double &z,
                       double &r, double &phi, double &theta) {
  const double R2 = x*x+y*y+z*z;
  if ( isAtOrigin(R2) ) {
    r = phi = theta = 0; // phi and theta can be arbitrary
  } else {
    r = std::sqrt(R2);
    xyz_to_pt(x/r, y/r, z/r, &phi, &theta);
  }
}

inline double ArcCsch(const double x) { // only handle x > 0
  return std::log((1.0+sqrt(1.0+x*x))/x);
}

inline int rank4Index(const int i, const int j, const int k, const int l) {
  return l+3*k+3*3*j+3*3*3*i;
}

inline int voigt_map(const int i, const int j) {
  if ( i == j ) {
    return i;
  }
  if ( (i == 1 && j == 2) || (i == 2 && j == 1) ) {
    return 3;
  }
  if ( (i == 0 && j == 2) || (i == 2 && j == 0) ) {
    return 4;
  }
  if ( (i == 0 && j == 1) || (i == 1 && j == 0) ) {
    return 5;
  }
  return -1;
}

inline Vector81d convertNtoC(const Matrix6d &N) {
  // Eigen::Matrix<double, 6, 9> E;
  // E <<
  //     1, 0, 0, 0, 0, 0, 0, 0, 0,
  //     0, 0, 0, 0, 1, 0, 0, 0, 0,
  //     0, 0, 0, 0, 0, 0, 0, 0, 1,
  //     0, 1, 0, 1, 0, 0, 0, 0, 0,
  //     0, 0, 1, 0, 0, 0, 1, 0, 0,
  //     0, 0, 0, 0, 0, 1, 0, 1, 0;  
  // const Matrix9d &&extN = E.transpose()*N*E;
  
  // Vector81d C;
  // for (int i = 0; i < 3; ++i) {
  //   for (int j = 0; j < 3; ++j) {
  //     for (int k = 0; k < 3; ++k) {
  //       for (int l = 0; l < 3; ++l) {
  //         C[rank4Index(i, j, k, l)] = extN(3*i+j, 3*k+l);
  //       }
  //     }
  //   }
  // }
  Vector81d C;
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      for (int k = 0; k < 3; ++k) {
        for (int l = 0; l < 3; ++l) {
          C[rank4Index(i, j, k, l)] = N(voigt_map(i, j), voigt_map(k, l));
        }
      }
    }
  }  
  return C;
}

inline void calc_lame_const(const double Ym, const double Pr,
                            double &mu, double &lambda) {
  mu = Ym/(2*(1+Pr));
  lambda = Ym*Pr/((1+Pr)*(1-2*Pr));
}

inline void vox_SF(double *val, const double *epsilon) {
  const double 
      tt1 = 1-epsilon[0],
      tt2 = 1-epsilon[1],
      tt3 = 1-epsilon[2],
      tt4 = epsilon[0]+1,
      tt5 = epsilon[1]+1,
      tt6 = epsilon[2]+1;
  
  val[0] = (tt1*tt2*tt3)/8.0;
  val[1] = (tt4*tt2*tt3)/8.0;
  val[2] = (tt1*tt5*tt3)/8.0;
  val[3] = (tt4*tt5*tt3)/8.0;
  val[4] = (tt1*tt2*tt6)/8.0;
  val[5] = (tt4*tt2*tt6)/8.0;
  val[6] = (tt1*tt5*tt6)/8.0;
  val[7] = (tt4*tt5*tt6)/8.0;
}

inline void vox_SF_jac(double *jac, const double *epsilon) {
  const double
      tt1 = 1-epsilon[1],
      tt2 = 1-epsilon[2],
      tt3 = epsilon[1]+1,
      tt4 = epsilon[2]+1,
      tt5 = 1-epsilon[0],
      tt6 = epsilon[0]+1;

  jac[0] = -(tt1*tt2)/8.0;
  jac[1] = (tt1*tt2)/8.0;
  jac[2] = -(tt3*tt2)/8.0;
  jac[3] = (tt3*tt2)/8.0;
  jac[4] = -(tt1*tt4)/8.0;
  jac[5] = (tt1*tt4)/8.0;
  jac[6] = -(tt3*tt4)/8.0;
  jac[7] = (tt3*tt4)/8.0;
  jac[8] = -(tt5*tt2)/8.0;
  jac[9] = -(tt6*tt2)/8.0;
  jac[10] = (tt5*tt2)/8.0;
  jac[11] = (tt6*tt2)/8.0;
  jac[12] = -(tt5*tt4)/8.0;
  jac[13] = -(tt6*tt4)/8.0;
  jac[14] = (tt5*tt4)/8.0;
  jac[15] = (tt6*tt4)/8.0;
  jac[16] = -(tt5*tt1)/8.0;
  jac[17] = -(tt6*tt1)/8.0;
  jac[18] = -(tt5*tt3)/8.0;
  jac[19] = -(tt6*tt3)/8.0;
  jac[20] = (tt5*tt1)/8.0;
  jac[21] = (tt6*tt1)/8.0;
  jac[22] = (tt5*tt3)/8.0;
  jac[23] = (tt6*tt3)/8.0;
}

}
#endif
