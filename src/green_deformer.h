#ifndef GREEN_DEFORMER_H
#define GREEN_DEFORMER_H

#include <boost/math/special_functions/spherical_harmonic.hpp>
#include "radial_func.h"

namespace green {

static Eigen::Matrix3d InvElasSymbol(const Vector81d &C, const Eigen::Vector3d &xi) {
  Eigen::Matrix3d ret = Eigen::Matrix3d::Zero();
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      for (int k = 0; k < 3; ++k) {
        for (int l = 0; l < 3; ++l) {
          ret(i, k) += C[rank4Index(i, j, k, l)]*xi[j]*xi[l];
        }
      }
    }
  }
  return ret.inverse();
}

// phi: [0, 2*pi], theta: [0, pi]

class GreenElasticDeformer
{
 public:  
  GreenElasticDeformer() : C_(Vector81d::Zero()) {
    // default radial functions
    rf_ = std::make_shared<singularRadialFunc>();
  }
  GreenElasticDeformer(const Vector81d &C, const int num_band, int order)
      : C_(C), num_band_(num_band) {
    //-> compute lebedev quadrature
    qx_.resize(order);
    qy_.resize(order);
    qz_.resize(order);
    qw_.resize(order);
    theta_.resize(order);
    phi_.resize(order);
    ld_by_order_(&order, &qx_[0], &qy_[0], &qz_[0], &qw_[0]);
    for (int i = 0; i < order; ++i) {
      xyz_to_tp_(&qx_[i], &qy_[i], &qz_[i], &phi_[i], &theta_[i]);
    }

    // default radial functions
    rf_ = std::make_shared<singularRadialFunc>();

    //-> compute spheric harmonic coefficient of C
    precompute();
  }
  int num_band() const { return num_band_; }
  void registerRadialFunc(const std::shared_ptr<radial_func_t> &rf) {
    rf_ = rf;
  }
  Matrix3d assembleG(const double r, const double theta, const double phi) const {
    // sum up over finite even bands
    Matrix3d ret = Matrix3d::Zero();
    for (int l = 0; l <= num_band_; l += 2) {
      const auto &Rl = band_constant(r, l, VECTOR);
      for (int m = 1; m <= l; ++m) {
        ret.noalias() += (2.0*Rl*Glm(r, theta, phi, l, m)).real();
      }
      ret.noalias() += (Rl*Glm(r, theta, phi, l, 0)).real();
    }
    return ret;
  }
  Matrix3d assembleG_l(const int l, const double r, const double theta, const double phi) const {
    Matrix3d ret = Matrix3d::Zero();
    if ( l > num_band_) {
      return ret;
    }
    if ( GlmCoeff_[l].size() == 0 ) { // odd band
      return ret;
    }
    const auto &Rl = band_constant(r, l, VECTOR); // band constant
    for (int m = 1; m <= l; ++m) {
      ret.noalias() += (2.0*Rl*Glm(r, theta, phi, l, m)).real();
    }
    ret.noalias() += (Rl*Glm(r, theta, phi, l, 0)).real();
    return ret;
  }
  Matrix93d assembleGradG(const double r, const double theta, const double phi) const {
    // sum up over finite odd bands
    Matrix93d ret = Matrix93d::Zero();
    for (int l = 1; l <= num_band_; l += 2) {
      const auto &Rl = band_constant(r, l, AFFINE);
      for (int m = 1; m <= l; ++m) {
        ret.noalias() += (2.0*Rl*GlmDiff(r, theta, phi, l, m)).real();
      }
      ret.noalias() += (Rl*GlmDiff(r, theta, phi, l, 0)).real();
    }
    return ret;
  }
  Matrix93d assembleGradG_l(const int l, const double r, const double theta, const double phi) const {
    Matrix93d ret = Matrix93d::Zero();
    if ( l > num_band_ ) {
      return ret;
    }
    if ( GlmCoeffDiff_[l].size() == 0 ) { // even band
      return ret;
    }
    const auto &Rl = band_constant(r, l, AFFINE); // band constant
    for (int m = 1; m <= l; ++m) {
      ret.noalias() += (2.0*Rl*GlmDiff(r, theta, phi, l, m)).real();
    }
    ret.noalias() += (Rl*GlmDiff(r, theta, phi, l, 0)).real();
    return ret;
  }
  int readAllG(const char *filename) {
    std::ifstream ifs(filename, std::ios::binary);
    if ( ifs.fail() ) {
      std::cerr << "# cannot open " << filename << std::endl;
      return __LINE__;
    }

    ifs.read((char *)&num_band_, sizeof(int));
    if ( !GlmCoeff_.empty() ) {
      GlmCoeff_.clear();
    }
    
    GlmCoeff_.resize(num_band_+1);
    for (int l = 0; l <= num_band_; l += 2) {
      for (int m = -l; m <= l; ++m) {
        Matrix3c G; Matrix3d real, imag;
        ifs.read((char *)real.data(), 3*3*sizeof(double));
        ifs.read((char *)imag.data(), 3*3*sizeof(double));
        G.real() = real;
        G.imag() = imag;
        GlmCoeff_[l].emplace_back(G);
      }
    }
    ifs.close();
    return 0;
  }
  int readAllGradG(const char *filename) {
    std::ifstream ifs(filename, std::ios::binary);
    if ( ifs.fail() ) {
      std::cerr << "# cannot open " << filename << std::endl;
      return __LINE__;
    }

    ifs.read((char *)&num_band_, sizeof(int));
    if ( !GlmCoeffDiff_.empty() ) {
      GlmCoeffDiff_.clear();
    }
    
    GlmCoeffDiff_.resize(num_band_+1);
    for (int l = 1; l <= num_band_; l += 2) {
      for (int m = -l; m <= l; ++m) {
        Matrix93c dG; Matrix93d real, imag;
        ifs.read((char *)real.data(), 9*3*sizeof(double));
        ifs.read((char *)imag.data(), 9*3*sizeof(double));
        dG.real() = real;
        dG.imag() = imag;
        GlmCoeffDiff_[l].emplace_back(dG);
      }
    }
    ifs.close();    
    return 0;
  }
  int writeAllG(const char *filename) const {
    std::ofstream ofs(filename, std::ios::binary);
    if ( ofs.fail() ) {
      std::cerr << "# cannot open " << filename << std::endl;
      return __LINE__;
    }

    ofs.write((const char *)&num_band_, sizeof(int));

    for (int l = 0; l <= num_band_; l += 2) {
      for (int m = -l; m <= l; ++m) {
        const Matrix3d &realG = GlmCoeff_[l][m+l].real(), &imagG = GlmCoeff_[l][m+l].imag();
        ofs.write((const char *)realG.data(), realG.size()*sizeof(double));
        ofs.write((const char *)imagG.data(), imagG.size()*sizeof(double));
      }
    }
    ofs.close();
    return 0;
  }
  int writeAllGradG(const char *filename) const {
    std::ofstream ofs(filename, std::ios::binary);
    if ( ofs.fail() ) {
      std::cerr << "# cannot open " << filename << std::endl;
      return __LINE__;
    }

    ofs.write((const char *)&num_band_, sizeof(int));
   
    for (int l = 1; l <= num_band_; l += 2) {
      for (int m = -l; m <= l; ++m) {
        const Matrix93d &realG = GlmCoeffDiff_[l][m+l].real(), &imagG = GlmCoeffDiff_[l][m+l].imag();
        ofs.write((const char *)realG.data(), realG.size()*sizeof(double));
        ofs.write((const char *)imagG.data(), imagG.size()*sizeof(double));
      }
    }
    ofs.close();
    return 0;
  }
  void debug() {
    spdlog::info("--- debug");

    // SH coeff of green's function
    for (int l = 0; l <= num_band_; ++l) {
      printf("----------------- band %d -------------------\n", l);
      for (int m = -l; m <= l; ++m) {
        std::cout << IntegrateGlmCoeff(l, m).norm() << " ";
      }
      std::cout << std::endl << std::endl;
    }
    std::cout << std::endl;

    for (int l = 0; l <= num_band_; ++l) {
      printf("----------------- band %d -------------------\n", l);
      for (int m = -l; m <= l; ++m) {
        std::cout << IntegrateGlmCoeffDiff(l, m).norm() << " ";
      }
      std::cout << std::endl << std::endl;
    }
    std::cout << std::endl;
  }
  void debug(const double r, const double theta, const double phi) {
    // SH coeff of green's function
    for (int l = 0; l <= num_band_; l += 2) {
      printf("----------------- band %d -------------------\n", l);
      for (int m = 0; m <= l; ++m) {
        std::cout << Glm(r, theta, phi, l, m) << std::endl << std::endl;
        std::cout << Glm(r, theta, phi, l, -m) << std::endl << std::endl;
      }
      std::cout << std::endl << std::endl;
    }
    std::cout << std::endl;

    for (int l = 1; l <= num_band_; l += 2) {
      printf("----------------- band %d -------------------\n", l);
      for (int m = 0; m <= l; ++m) {
        std::cout << GlmDiff(r, theta, phi, l, m) << std::endl << std::endl;
        std::cout << GlmDiff(r, theta, phi, l, -m) << std::endl << std::endl;        
      }
      std::cout << std::endl << std::endl;
    }
    std::cout << std::endl;    
  }

 private:
  Matrix3c IntegrateGlmCoeff(const int l, const int m) {
    Matrix3c ret = Matrix3c::Zero();
    for (int i = 0; i < qw_.size(); ++i) {
      const auto &&inv_symbol = InvElasSymbol(C_, Eigen::Vector3d(qx_[i], qy_[i], qz_[i]));
      ret.noalias() += qw_[i]*inv_symbol*std::conj(boost::math::spherical_harmonic(l, m, theta_[i], phi_[i]));
    }
    return 4*M_PI*ret;
  }
  Matrix93c IntegrateGlmCoeffDiff(const int l, const int m) {
    using namespace std::complex_literals;

    Matrix93c ret = Matrix93c::Zero();

    #pragma omp parallel sections
    {
      #pragma omp section
      {
        for (int i = 0; i < qw_.size(); ++i) {
          const auto &&inv_symbol = 1i*qx_[i]*InvElasSymbol(C_, Eigen::Vector3d(qx_[i], qy_[i], qz_[i]));
          ret.topRows(3) += qw_[i]*inv_symbol*std::conj(boost::math::spherical_harmonic(l, m, theta_[i], phi_[i]));
        }
      }

      #pragma omp section
      {
        for (int i = 0; i < qw_.size(); ++i) {
          const auto &&inv_symbol = 1i*qy_[i]*InvElasSymbol(C_, Eigen::Vector3d(qx_[i], qy_[i], qz_[i]));
          ret.middleRows(3, 3) += qw_[i]*inv_symbol*std::conj(boost::math::spherical_harmonic(l, m, theta_[i], phi_[i]));
        }
      }

      #pragma omp section
      {
        for (int i = 0; i < qw_.size(); ++i) {
          const auto &&inv_symbol = 1i*qz_[i]*InvElasSymbol(C_, Eigen::Vector3d(qx_[i], qy_[i], qz_[i]));
          ret.bottomRows(3) += qw_[i]*inv_symbol*std::conj(boost::math::spherical_harmonic(l, m, theta_[i], phi_[i]));
        }         
      }
    }
      
    return 4*M_PI*ret;
  }
  void precompute() {
    spdlog::info("--- precompute");

    // SH coeff of green's function
    GlmCoeff_.resize(num_band_+1);
    for (int l = 0; l <= num_band_; l += 2) {
      GlmCoeff_[l].resize(2*l+1);
      #pragma omp parallel for
      for (int m = -l; m <= l; ++m) {
        GlmCoeff_[l][m+l] = IntegrateGlmCoeff(l, m);
      }
    }
    for (int l = 0; l <= num_band_; l += 2) {
      printf("----------------- band %d -------------------\n", l);
      for (int m = -l; m <= l; ++m) {
        std::cout << GlmCoeff_[l][m+l].norm() << " ";
      }
      std::cout << std::endl << std::endl;
    }
    std::cout << std::endl;

    // SH coeff of green's function gradient
    GlmCoeffDiff_.resize(num_band_+1);
    for (int l = 1; l <= num_band_; l += 2) {
      GlmCoeffDiff_[l].resize(2*l+1);
      #pragma omp parallel for
      for (int m = -l; m <= l; ++m) {
        GlmCoeffDiff_[l][m+l] = IntegrateGlmCoeffDiff(l, m);
      }
    }
    for (int l = 1; l <= num_band_; l += 2) {
      printf("----------------- band %d -------------------\n", l);
      for (int m = -l; m <= l; ++m) {
        std::cout << GlmCoeffDiff_[l][m+l].norm() << " ";
      }
      std::cout << std::endl << std::endl;
    }
    std::cout << std::endl;
  }
  Matrix3c Glm(const double r, const double theta, const double phi,
               const int l, const int m) const { // coordinate dependent    
    if ( GlmCoeff_[l][m+l].squaredNorm() < 1e-12 ) {
      return Matrix3c::Zero();
    }    
    return boost::math::spherical_harmonic(l, m, theta, phi)*GlmCoeff_[l][m+l];
  }
  Matrix93c GlmDiff(const double r, const double theta, const double phi,
                    const int l, const int m) const {
    if ( GlmCoeffDiff_[l][m+l].squaredNorm() < 1e-12 ) {
      return Matrix93c::Zero();
    }
    return boost::math::spherical_harmonic(l, m, theta, phi)*GlmCoeffDiff_[l][m+l];
  }
  complex_t band_constant(const double r, const int l, load_type d) const {
    using namespace std::complex_literals;  
    return INV_2PI2*std::pow(1i, l)*(*rf_)(l, r, d);
  }
  
 private:
  const Vector81d C_;
  int num_band_;
  const double INV_2PI2 = 1.0/(2*M_PI*M_PI);

  std::shared_ptr<radial_func_t> rf_;

  std::vector<double> qx_, qy_, qz_, qw_;
  std::vector<double> theta_, phi_;

  std::vector<std::vector<Matrix3c>> GlmCoeff_;
  std::vector<std::vector<Matrix93c>> GlmCoeffDiff_;
};

}
#endif
