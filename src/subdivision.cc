#include <iostream>
#include <memory>

#include "subdivision.h"
#include "util.h"
#include "macro.h"

using namespace std;
using namespace Eigen;

namespace green {

static const int VOX_EDGE_ORDER[12][2] = {{0, 1}, {1, 3}, {3, 2}, {2, 0},
                                          {4, 5}, {5, 7}, {7, 6}, {6, 4},
                                          {0, 4}, {1, 5}, {3, 7}, {2, 6}};

static const int VOX_FACE_ORDER[6][4] = {{0, 1, 3, 2}, {1, 0, 4, 5},
                                         {5, 4, 6, 7}, {7, 6, 2, 3},
                                         {2, 6, 4, 0}, {7, 3, 1, 5}};

void collect_one_vox_edges(const veci_t &vox, vector<vector<size_t>> &edges) {
  edges.resize(12);
  for (size_t i = 0; i < 12; ++i) {
    for (size_t j = 0; j < 2; ++j)
      edges[i].push_back(vox[VOX_EDGE_ORDER[i][j]]);
    if ( edges[i][0] > edges[i][1] )
      std::swap(edges[i][0], edges[i][1]);
  }
}

void collect_one_vox_faces(const veci_t &vox, vector<vector<size_t>> &faces) {
  faces.resize(6);
  for (size_t i = 0; i < 6; ++i) {
    for (size_t j = 0; j < 4; ++j) {
      faces[i].emplace_back(vox[VOX_FACE_ORDER[i][j]]);
    }
  }
}

//===============================================================================
vox_edges::vox_edges(const mati_t &cell) {
  size_t cnt = 0;
  for (size_t i = 0; i < cell.cols(); ++i) {
    vector<vector<size_t>> edges;
    collect_one_vox_edges(cell.col(i), edges);

    for (size_t j = 0; j < edges.size(); ++j) {
      if ( edge2idx_.find(edges[j]) == edge2idx_.end() )
        edge2idx_.insert(make_pair(edges[j], cnt++));
    }
  }
}

size_t vox_edges::edges_num() const {
  return edge2idx_.size();
}

size_t vox_edges::query_edge_idx(const size_t p, const size_t q) const {
  const vector<size_t> pq = {p, q};
  return this->query_edge_idx(pq);
}

size_t vox_edges::query_edge_idx(const vector<size_t> &ab) const {
  vector<size_t> key(ab);
  if ( key[0] > key[1] )
    std::swap(key[0], key[1]);
  const auto it = edge2idx_.find(key);
  return (it == edge2idx_.end() ? -1 : it->second);
}

//===============================================================================
vox_faces::vox_faces(const mati_t &cell) {
  size_t cnt = 0;
  for (size_t i = 0; i < cell.cols(); ++i) {
    vector<vector<size_t>> faces;
    collect_one_vox_faces(cell.col(i), faces);

    for (size_t j = 0; j < faces.size(); ++j) {
      vector<size_t> ordered_face = faces[j];
      std::sort(faces[j].begin(), faces[j].end());

      if ( face2idx_.find(faces[j]) == face2idx_.end() ) { // first recorded
        face2idx_.insert(make_pair(faces[j], cnt++));
        face2vox_.insert(make_pair(faces[j], vector<size_t>()));
        face_in_order_.insert(make_pair(faces[j], ordered_face));
      }
      face2vox_[faces[j]].push_back(i);
    }
  }
}

size_t vox_faces::faces_num() const {
  return face2idx_.size();
}

size_t vox_faces::query_face_idx(const size_t p, const size_t q, const size_t m,  const size_t n) const {
  const vector<size_t> pqmn = {p, q, m, n};
  return this->query_face_idx(pqmn);
}

size_t vox_faces::query_face_idx(const vector<size_t> &abcd) const {
  vector<size_t> key(abcd);
  std::sort(key.begin(), key.end());
  const auto it = face2idx_.find(key);
  return (it == face2idx_.end() ? -1 : it->second);
}

//===============================================================================
int subdivide_vox(const mati_t &voxs, const matd_t &nods,
                  mati_t &new_voxs, matd_t &new_nods) {
  std::shared_ptr<vox_edges> ptre = make_shared<vox_edges>(voxs);
  std::shared_ptr<vox_faces> ptrf = make_shared<vox_faces>(voxs);

  const size_t new_elem_num = 8*voxs.cols();
  const size_t new_nods_num = nods.cols()+ptre->edges_num()+ptrf->faces_num()+voxs.cols();

  new_voxs.resize(8, new_elem_num);
  new_nods.resize(3, new_nods_num);

  vector<Eigen::Triplet<double>> trips, tripsW;
  
  // construct nods
  size_t curr_nods_num = nods.cols();
  new_nods.leftCols(curr_nods_num) = nods;

  size_t cnt_vert_only_on_fine = 0;
  for (auto &entry : ptre->edge2idx_) {
    new_nods.col(curr_nods_num+entry.second)
        = 0.5*(nods.col(entry.first[0])+nods.col(entry.first[1]));
  }
  curr_nods_num += ptre->edges_num();
  for (auto &entry : ptrf->face2idx_) {
    new_nods.col(curr_nods_num+entry.second)
        = 0.25*(nods.col(entry.first[0])+nods.col(entry.first[1])+nods.col(entry.first[2])+nods.col(entry.first[3]));
  }
  curr_nods_num += ptrf->faces_num();
  for (size_t i = 0; i < voxs.cols(); ++i) {
    new_nods.col(curr_nods_num+i) =
        0.125*(
        nods.col(voxs(0, i))+
        nods.col(voxs(1, i))+
        nods.col(voxs(2, i))+        
        nods.col(voxs(3, i))+
        nods.col(voxs(4, i))+
        nods.col(voxs(5, i))+
        nods.col(voxs(6, i))+        
        nods.col(voxs(7, i)));
  }  
  curr_nods_num += voxs.cols();
  ASSERT(curr_nods_num == new_nods_num);

  const size_t edge_vert_offset = nods.cols();
  const size_t face_vert_offset = edge_vert_offset+ptre->edges_num();
  const size_t elem_vert_offset = face_vert_offset+ptrf->faces_num();
  
  // construct connectivity
  #pragma omp parallel for
  for (size_t i = 0; i < voxs.cols(); ++i) {
    const auto &x = voxs.col(i);
    
    new_voxs(0, 8*i+0) = x[0];
    new_voxs(1, 8*i+0) = ptre->query_edge_idx(x[0], x[1])+edge_vert_offset;
    new_voxs(2, 8*i+0) = ptre->query_edge_idx(x[0], x[2])+edge_vert_offset;
    new_voxs(3, 8*i+0) = ptrf->query_face_idx(x[0], x[1], x[2], x[3])+face_vert_offset;
    new_voxs(4, 8*i+0) = ptre->query_edge_idx(x[0], x[4])+edge_vert_offset;
    new_voxs(5, 8*i+0) = ptrf->query_face_idx(x[0], x[1], x[4], x[5])+face_vert_offset;
    new_voxs(6, 8*i+0) = ptrf->query_face_idx(x[0], x[2], x[6], x[4])+face_vert_offset;
    new_voxs(7, 8*i+0) = i+elem_vert_offset;

    new_voxs(0, 8*i+1) = ptre->query_edge_idx(x[0], x[1])+edge_vert_offset;
    new_voxs(1, 8*i+1) = x[1];
    new_voxs(2, 8*i+1) = ptrf->query_face_idx(x[0], x[1], x[2], x[3])+face_vert_offset;
    new_voxs(3, 8*i+1) = ptre->query_edge_idx(x[1], x[3])+edge_vert_offset;
    new_voxs(4, 8*i+1) = ptrf->query_face_idx(x[0], x[1], x[4], x[5])+face_vert_offset;
    new_voxs(5, 8*i+1) = ptre->query_edge_idx(x[1], x[5])+edge_vert_offset;
    new_voxs(6, 8*i+1) = i+elem_vert_offset;
    new_voxs(7, 8*i+1) = ptrf->query_face_idx(x[1], x[3], x[7], x[5])+face_vert_offset;

    new_voxs(0, 8*i+2) = ptre->query_edge_idx(x[0], x[2])+edge_vert_offset;
    new_voxs(1, 8*i+2) = ptrf->query_face_idx(x[0], x[1], x[2], x[3])+face_vert_offset;
    new_voxs(2, 8*i+2) = x[2];
    new_voxs(3, 8*i+2) = ptre->query_edge_idx(x[2], x[3])+edge_vert_offset;
    new_voxs(4, 8*i+2) = ptrf->query_face_idx(x[0], x[2], x[6], x[4])+face_vert_offset;
    new_voxs(5, 8*i+2) = i+elem_vert_offset;
    new_voxs(6, 8*i+2) = ptre->query_edge_idx(x[2], x[6])+edge_vert_offset;
    new_voxs(7, 8*i+2) = ptrf->query_face_idx(x[2], x[3], x[7], x[6])+face_vert_offset;

    new_voxs(0, 8*i+3) = ptrf->query_face_idx(x[0], x[1], x[2], x[3])+face_vert_offset;
    new_voxs(1, 8*i+3) = ptre->query_edge_idx(x[1], x[3])+edge_vert_offset;
    new_voxs(2, 8*i+3) = ptre->query_edge_idx(x[2], x[3])+edge_vert_offset;
    new_voxs(3, 8*i+3) = x[3];
    new_voxs(4, 8*i+3) = i+elem_vert_offset;
    new_voxs(5, 8*i+3) = ptrf->query_face_idx(x[1], x[3], x[7], x[5])+face_vert_offset;
    new_voxs(6, 8*i+3) = ptrf->query_face_idx(x[2], x[3], x[7], x[6])+face_vert_offset;
    new_voxs(7, 8*i+3) = ptre->query_edge_idx(x[3], x[7])+edge_vert_offset;

    new_voxs(0, 8*i+4) = ptre->query_edge_idx(x[0], x[4])+edge_vert_offset;
    new_voxs(1, 8*i+4) = ptrf->query_face_idx(x[0], x[1], x[5], x[4])+face_vert_offset;
    new_voxs(2, 8*i+4) = ptrf->query_face_idx(x[0], x[2], x[6], x[4])+face_vert_offset;
    new_voxs(3, 8*i+4) = i+elem_vert_offset;
    new_voxs(4, 8*i+4) = x[4];
    new_voxs(5, 8*i+4) = ptre->query_edge_idx(x[4], x[5])+edge_vert_offset;
    new_voxs(6, 8*i+4) = ptre->query_edge_idx(x[4], x[6])+edge_vert_offset;
    new_voxs(7, 8*i+4) = ptrf->query_face_idx(x[4], x[5], x[6], x[7])+face_vert_offset;

    new_voxs(0, 8*i+5) = ptrf->query_face_idx(x[0], x[1], x[4], x[5])+face_vert_offset;
    new_voxs(1, 8*i+5) = ptre->query_edge_idx(x[1], x[5])+edge_vert_offset;
    new_voxs(2, 8*i+5) = i+elem_vert_offset;
    new_voxs(3, 8*i+5) = ptrf->query_face_idx(x[1], x[3], x[7], x[5])+face_vert_offset;
    new_voxs(4, 8*i+5) = ptre->query_edge_idx(x[4], x[5])+edge_vert_offset;
    new_voxs(5, 8*i+5) = x[5];
    new_voxs(6, 8*i+5) = ptrf->query_face_idx(x[4], x[5], x[6], x[7])+face_vert_offset;
    new_voxs(7, 8*i+5) = ptre->query_edge_idx(x[5], x[7])+edge_vert_offset;

    new_voxs(0, 8*i+6) = ptrf->query_face_idx(x[0], x[2], x[6], x[4])+face_vert_offset;
    new_voxs(1, 8*i+6) = i+elem_vert_offset;
    new_voxs(2, 8*i+6) = ptre->query_edge_idx(x[2], x[6])+edge_vert_offset;
    new_voxs(3, 8*i+6) = ptrf->query_face_idx(x[2], x[3], x[7], x[6])+face_vert_offset;
    new_voxs(4, 8*i+6) = ptre->query_edge_idx(x[4], x[6])+edge_vert_offset;
    new_voxs(5, 8*i+6) = ptrf->query_face_idx(x[4], x[5], x[6], x[7])+face_vert_offset;
    new_voxs(6, 8*i+6) = x[6];
    new_voxs(7, 8*i+6) = ptre->query_edge_idx(x[6], x[7])+edge_vert_offset;

    new_voxs(0, 8*i+7) = i+elem_vert_offset;
    new_voxs(1, 8*i+7) = ptrf->query_face_idx(x[1], x[3], x[7], x[5])+face_vert_offset;
    new_voxs(2, 8*i+7) = ptrf->query_face_idx(x[2], x[3], x[7], x[6])+face_vert_offset;
    new_voxs(3, 8*i+7) = ptre->query_edge_idx(x[3], x[7])+edge_vert_offset;
    new_voxs(4, 8*i+7) = ptrf->query_face_idx(x[4], x[5], x[6], x[7])+face_vert_offset;
    new_voxs(5, 8*i+7) = ptre->query_edge_idx(x[5], x[7])+edge_vert_offset;
    new_voxs(6, 8*i+7) = ptre->query_edge_idx(x[6], x[7])+edge_vert_offset;
    new_voxs(7, 8*i+7) = x[7];
  }
  return 0;
}

}
