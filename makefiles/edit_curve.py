"""
===========
Path Editor
===========

Sharing events across GUIs.

This example demonstrates a cross-GUI application using Matplotlib event
handling to interact with and modify objects on the canvas.
"""

import numpy as np
from  scipy.interpolate import interp1d
from matplotlib.backend_bases import MouseButton
from matplotlib.lines import Line2D
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import matplotlib.pyplot as plt
import os, math, sys
from scipy.special import gamma, hyp1f1
import matplotlib

def interpolate(x, y):
    x, y = x, y
    i = np.arange(len(x))

    interp_i = np.linspace(0, i.max(), 100 * i.max())

    xi = interp1d(i, x, kind='cubic')(interp_i)
    yi = interp1d(i, y, kind='cubic')(interp_i)

    return xi,yi

def dist(x, y):
    """
    Return the distance between two points.
    """
    d = x - y
    return np.sqrt(np.dot(d, d))

def dist_point_to_segment(p, s0, s1):
    """
    Get the distance of a point to a segment.
      *p*, *s0*, *s1* are *xy* sequences
    This algorithm from
    http://geomalgorithms.com/a02-_lines.html
    """
    v = s1 - s0
    w = p - s0
    c1 = np.dot(w, v)
    if c1 <= 0:
        return dist(p, s0)
    c2 = np.dot(v, v)
    if c2 <= c1:
        return dist(p, s1)
    b = c1 / c2
    pb = s0 + b * v
    return dist(p, pb)

class PathInteractor:
    """
    An path editor.

    Press 't' to toggle vertex markers on and off.  When vertex markers are on,
    they can be dragged with the mouse.
    """

    showverts = True
    epsilon = 5  # max pixel distance to count as a vertex hit

    def __init__(self, spline, verts):

        self.ax = spline.axes###pathpatch.axes
        canvas = self.ax.figure.canvas
        self.verts = verts
        self.spline = spline
        self.spline.set_animated(True)

        x, y = zip(*self.verts)

        self.line, = ax.plot(x, y, marker='o', markersize=7, markerfacecolor='none', ls = '--', linewidth=0, animated=True)

        self._ind = None  # the active vertex

        canvas.mpl_connect('draw_event', self.on_draw)
        canvas.mpl_connect('button_press_event', self.on_button_press)
        canvas.mpl_connect('key_press_event', self.on_key_press)
        canvas.mpl_connect('button_release_event', self.on_button_release)
        canvas.mpl_connect('motion_notify_event', self.on_mouse_move)
        self.canvas = canvas

        np.savetxt(outfile, np.array(self.verts), delimiter=' ')

    def get_ind_under_point(self, event):
        """
        Return the index of the point closest to the event position or *None*
        if no point is within ``self.epsilon`` to the event position.
        """
        # display coords
        #xy = verts #np.asarray(self.pathpatch.get_path().vertices)
        xyt = self.spline.get_transform().transform(self.verts)
        xt, yt = xyt[:, 0], xyt[:, 1]
        d = np.sqrt((xt - event.x)**2 + (yt - event.y)**2)
        ind = d.argmin()

        if d[ind] >= self.epsilon:
            ind = None

        return ind

    def on_draw(self, event):
        """Callback for draws."""
        self.background = self.canvas.copy_from_bbox(self.ax.bbox)
        self.ax.draw_artist(self.spline)
        self.ax.draw_artist(self.line)
        self.canvas.blit(self.ax.bbox)

    def on_button_press(self, event):
        """Callback for mouse button presses."""
        if (event.inaxes is None
                or event.button != MouseButton.LEFT
                or not self.showverts):
            return
        self._ind = self.get_ind_under_point(event)

    def on_button_release(self, event):
        """Callback for mouse button releases."""
        if (event.button != MouseButton.LEFT
                or not self.showverts):
            return
        self._ind = None

    def on_key_press(self, event):
        """Callback for key presses."""
        if not event.inaxes:
            return
        if event.key == 't':
            self.showverts = not self.showverts
            self.line.set_visible(self.showverts)
            if not self.showverts:
                self._ind = None
        elif event.key == 'i':
            xys = self.spline.get_transform().transform(verts)
            p = event.x, event.y
            for i in range(len(xys) - 1):
                s0 = xys[i]
                s1 = xys[i + 1]
                d = dist_point_to_segment(p, s0, s1)
                if d <= self.epsilon:
                    self.verts.insert(i+1, (event.xdata, event.ydata))
                    self.line.set_data(zip(*self.verts))
                    x, y = interpolate(*zip(*self.verts))
                    self.spline.set_data(x, y)

        self.canvas.draw()

    def on_mouse_move(self, event):
        """Callback for mouse movements."""
        if (self._ind is None
                or event.inaxes is None
                or event.button != MouseButton.LEFT
                or not self.showverts):
            return

        self.verts[self._ind] = event.xdata, event.ydata
        self.line.set_data(zip(*self.verts))
        x, y = interpolate(*zip(*self.verts))
        self.spline.set_data(x, y)

        np.savetxt(outfile, np.array(self.verts), delimiter=' ')
        # os.system('../build/examples/cubic_spline cubic_control.txt > cubic_points.txt')
        # os.system('python3 plot_curve.py')

        self.canvas.restore_region(self.background)
        self.ax.draw_artist(self.spline)
        self.ax.draw_artist(self.line)
        self.canvas.blit(self.ax.bbox)

def PointR(r, l, eps):
    return (math.sqrt(np.pi) * r**l * eps**(-1-l) * gamma((l+1)/2.) * hyp1f1((l+1)/2., 1.5+l, -r**2/eps**2))/gamma(1.5+l)/2.

def AffineR(r, l, eps):
    return math.sqrt(np.pi) * r**l * eps**(-2 - l) * gamma(1 + l/2.) * hyp1f1(1 + l/2.,1.5 + l,-(r**2/eps**2))/gamma(1.5+l)

if __name__ == "__main__":
    eps = 0.2
    upper_x = 3.0
    n = 30
    
    l = int(sys.argv[1])
    outfile = 'band-{}.xy'.format(l)

    plt.rcParams.update({
        "text.usetex": True,
        #"text.latex.preamble": r"\usepackage{amsmath}",
        #"font.family": "serif",
        "font.size": 40,
        #"font.weight": "bold",
        #"axes.labelweight": "bold"
    })
    
    fig = plt.figure(figsize=(6,3.5), dpi=200)
    ax = fig.add_subplot(111)

    dx = upper_x/(n-1)
    
    # even l for point-wise load, odd l for affine loads
    RadialF = PointR if l%2 == 0 else AffineR

    verts = []
    max_y = 0.    
    for i in range(n):
        val_y = RadialF(i*dx, l, eps)
        max_y = max(max_y, val_y)
        verts.append((i*dx, val_y))

    x, y = interpolate(*zip(*verts))
    spline = Line2D(x,y, linewidth=4)
    ax.add_line(spline)
    interactor = PathInteractor(spline, verts)

    ax.set_title(r'$R_{}(r)$'.format(l), x=0.8, y=0.6) 
    ax.set_xlim(-0.3, upper_x+0.3)
    ax.set_ylim(-2, 1.2*max_y)
#    ax.tick_params(axis='x', direction='in', pad=-30)
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(4)

        
    ax.tick_params(direction='in', length=6, width=2,
                  grid_color='r', grid_alpha=0.5)

    plt.xticks([0, upper_x], [0, upper_x])
    plt.yticks([0, int(max_y)], [0, int(max_y)])
    plt.tight_layout()
    plt.show()
