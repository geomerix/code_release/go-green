#!/bin/bash

for L in 1; do
    for A in 0.0 1.0 2.0 3.0; do
	python scale_curves.py ${L} ${A} 1.0
	./run_dna.sh ${L} ${A}
    done
done


for L in 3; do
    for A in 0.0 3.3 6.6 10.0; do
	python scale_curves.py ${L} ${A} 1.0
	./run_dna.sh ${L} ${A}
    done
done


for L in 5; do
    for A in 0.0 10.0 20.0 30.0; do
	python scale_curves.py ${L} ${A} 1.0
	./run_dna.sh ${L} ${A}
    done
done
