#!/bin/bash

num_threads=`echo "$(cat /proc/cpuinfo | grep processor | wc -l)/2" | bc`
echo "threads=${num_threads}"
export OMP_NUM_THREADS=${num_threads}

mtr_name=ABBA
max_e=100
min_e=100

reg_func=spline
reg_eps=0.2

num_band=8

ref_mesh=../data/31_horse_dense.obj
edt_mesh=../data/31_horse_dense-edit-0.obj
force_scale=150
affine_scale=-2000

cx=1.0
cy=0.5
cz=0.6

basedir=../result/green/$(date -I)/
make -f pipeline.mk spl BASEDIR=${basedir} MTR_NAME=${mtr_name} MAX_E=${max_e} MIN_E=${min_e} NUM_BAND=${num_band} REG_FUNC=${reg_func} REG_EPS=${reg_eps} DEMO_REF_MESH=${ref_mesh} DEMO_EDT_MESH=${edt_mesh} FORCE_SCALE=${force_scale} AFFINE_SCALE=${affine_scale} SPL_SUFFIX=${1} SPL_CX=${cx} SPL_CY=${cy} SPL_CZ=${cz} SPL_L=1 SPL_A=1 BAND0_XY=horse-band-0.xy BAND1_XY=horse-band-1.xy
