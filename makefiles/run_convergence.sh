#!/bin/bash

num_threads=`echo "$(cat /proc/cpuinfo | grep processor | wc -l)/2" | bc`
echo "threads=${num_threads}"
export OMP_NUM_THREADS=${num_threads}

for mtr_name in SINE01 BONE ABBA HOMO; do
max_e=100
min_e=1

reg_func=gaussian
reg_eps=0.4
domain_scale=1.0

num_band=100
num_order=5810

basedir=../result/green/$(date -I)-CONV/
make -f pipeline.mk green PROG=basic_convergence BASEDIR=${basedir} MTR_NAME=${mtr_name} MAX_E=${max_e} MIN_E=${min_e} NUM_BAND=${num_band} REG_FUNC=${reg_func} REG_EPS=${reg_eps} DOMAIN_SCALE=${domain_scale} NUM_ORDER=${num_order}

done
