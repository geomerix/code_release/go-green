import numpy as np
import matplotlib.pyplot as plt
import sys
from matplotlib.lines import Line2D
from edit_curve import interpolate

if __name__ == "__main__":
    l = int(sys.argv[1])
    input_file = 'band-{}.xy'.format(l)

#    if input

    plt.rcParams.update({
        "text.usetex": True,
        "font.size": 40,
        'text.latex.preamble': r'\usepackage{amsmath}'
    })
    
    fig = plt.figure(figsize=(6,3.5), dpi=200)
    ax = fig.add_subplot(111)

    verts = np.genfromtxt(input_file, delimiter=' ')
    print(verts.shape)
    max_x, max_y, min_y = np.max(verts[:,0]), np.max(verts[:,1]), np.min(verts[:,1])
    print(max_x, max_y)

    x, y = interpolate(*zip(*verts))
    spline = Line2D(x, y, linewidth=4)
    ax.add_line(spline)
    ax.plot(verts[:,0], verts[:,1], marker='o', markersize=7, markerfacecolor='none', ls = '--', linewidth=0, animated=True)    

    if l%2 == 0:
        ax.set_title(r'$R_{}(r)$'.format(l), x=0.8, y=0.6)
    else:
        ax.set_title(r'$\mathcal R_{}(r)$'.format(l), x=0.8, y=0.6)                
            
    ax.set_xlim(0-0.3, max_x+0.3)
    ax.set_ylim(min_y-0.3, 1.1*max_y)
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(4)
        
    ax.tick_params(direction='in', length=6, width=2, grid_color='r', grid_alpha=0.5)

    plt.xticks([0, int(max_x)], [0, int(max_x)])
    if max_y < 1.:
        plt.yticks([0, 0.2], [0, 0.2])
    else:
        plt.yticks([0, int(max_y)], [0, int(max_y)])
    
    plt.tight_layout()

    plt.savefig('band-{}.png'.format(l), dpi=500)
