#!/bin/bash

num_threads=`echo "$(cat /proc/cpuinfo | grep processor | wc -l)/2" | bc`
echo "threads=${num_threads}"
export OMP_NUM_THREADS=${num_threads}

mtr_name=ABBA
max_e=500
min_e=5

reg_func=spline
reg_eps=1.6

num_band=8
num_order=194
    
ref_mesh=../data/dna.obj
edt_mesh=../data/dna.obj
force_scale=0
affine_scale=1000

cx=0.2
cy=0.0
cz=0.0

f00=-1
f01=0
f02=0
f10=0
f11=1
f12=0
f20=0
f21=0
f22=0

basedir=../result/green/$(date -I)-DNA/
make -f pipeline.mk spl BASEDIR=${basedir} MTR_NAME=${mtr_name} MAX_E=${max_e} MIN_E=${min_e} NUM_BAND=${num_band} REG_FUNC=${reg_func} REG_EPS=${reg_eps} DEMO_REF_MESH=${ref_mesh} DEMO_EDT_MESH=${edt_mesh} FORCE_SCALE=${force_scale} AFFINE_SCALE=${affine_scale} SPL_SUFFIX=${1} SPL_CX=${cx} SPL_CY=${cy} SPL_CZ=${cz} NUM_ORDER=${num_order} F00=${f00} F01=${f01} F02=${f02} F10=${f10} F11=${f11} F12=${f12} F20=${f20} F21=${f21} F22=${f22} SPL_L=${1} SPL_A=${2}
