# subroutines
HOMOG_EXEC = ../build/examples/homog_material
PREPR_EXEC = ../build/examples/precompute_sh
GREEN_EXEC = ../build/examples/test_green_fct
DEMO_EXEC  = ../build/examples/test_green_demo
CONS_EXEC  = ../build/examples/test_green_cons
SPL_EXEC   = ../build/examples/spline_editing

# config materials
MTR_NAME ?=
MAX_E ?=
MIN_E ?=
POISSON_RATIO ?= 0.4
MTR_IMG_FILE ?= 

# number of SH degrees and quadrature orders
NUM_BAND ?=
NUM_ORDER ?= 194

# knots of splines for each SH degree
BAND0_XY ?= band-0.xy
BAND1_XY ?= band-1.xy
BAND2_XY ?= band-2.xy
BAND3_XY ?= band-3.xy
BAND4_XY ?= band-4.xy
BAND5_XY ?= band-5.xy

PROG ?= 
REG_FUNC ?= 
REG_EPS ?= 
DOMAIN_SCALE ?=
NUM_THREADS ?= $(OMP_NUM_THREADS)

BASEDIR ?=
OUTDIR ?= $(BASEDIR)/$(MTR_NAME)-$(MAX_E)-$(MIN_E)/

DEMO_REF_MESH ?=
DEMO_EDT_MESH ?=
FORCE_SCALE ?=
AFFINE_SCALE ?=
MODEL_NAME = $(basename $(notdir $(DEMO_REF_MESH)))
$(info $$MODEL_NAME is [${MODEL_NAME}])

HOMOG_OUTPUT = $(OUTDIR)/C.mat
PREPR_OUTPUT = $(OUTDIR)/allG.mat
GREEN_OUTPUT = $(OUTDIR)/$(PROG)/config-green.json
DEMO_OUTPUT  = $(OUTDIR)/$(MODEL_NAME)/deform_mesh-$(FORCE_SCALE).obj
CONS_OUTPUT  = $(OUTDIR)/$(MODEL_NAME)/cons_deform_mesh.obj
SPL_OUTPUT   = $(OUTDIR)/$(MODEL_NAME)/editing_result.obj
SPL_SUFFIX ?= 

SPL_CX ?= 0
SPL_CY ?= 0
SPL_CZ ?= 0
F00 ?= -1
F01 ?= 0
F02 ?= 0
F10 ?= 0
F11 ?= -1
F12 ?= 0
F20 ?= 0
F21 ?= 0
F22 ?= -1

SPL_L ?=
SPL_A ?=

homog: $(HOMOG_OUTPUT)
prepr: $(PREPR_OUTPUT)
green: $(GREEN_OUTPUT)
demo: $(DEMO_OUTPUT)
cons: $(CONS_OUTPUT)
spl: $(SPL_OUTPUT)

# homogenize composite materials
$(HOMOG_OUTPUT): $(HOMOG_EXEC)
	@mkdir -p $(OUTDIR)
	$(HOMOG_EXEC) outdir=$(OUTDIR) scale_gh=0.2 max_E=$(MAX_E) min_E=$(MIN_E) poisson_ratio=$(POISSON_RATIO) mtr_name=$(MTR_NAME) img_file=$(MTR_IMG_FILE) 2>&1 | tee $(OUTDIR)/homog_log.txt

# precomputate material terms P_l^m
$(PREPR_OUTPUT): $(PREPR_EXEC) $(HOMOG_OUTPUT)
	@mkdir -p $(OUTDIR)
	$(PREPR_EXEC) outdir=$(OUTDIR) material=$(HOMOG_OUTPUT) num_band=$(NUM_BAND) num_order=$(NUM_ORDER) 2>&1 | tee $(OUTDIR)/precompute_log.txt

$(GREEN_OUTPUT): $(PREPR_OUTPUT) $(GREEN_EXEC) $(BAND0_XY)
	@mkdir -p $(OUTDIR)/$(PROG)
	$(GREEN_EXEC) prog=$(PROG) outdir=$(OUTDIR)/$(PROG) file_allG=$(PREPR_OUTPUT) file_allGradG=$(OUTDIR)/allGradG.mat reg_func=$(REG_FUNC) reg_eps=$(REG_EPS) domain_scale=$(DOMAIN_SCALE) BAND0_XY=$(BAND0_XY) BAND1_XY=$(BAND1_XY) BAND2_XY=$(BAND2_XY) BAND3_XY=$(BAND3_XY) BAND4_XY=$(BAND4_XY) BAND5_XY=$(BAND5_XY) num_threads=$(NUM_THREADS) 2>&1 | tee $(OUTDIR)/$(PROG)/green_log.txt

$(DEMO_OUTPUT): $(PREPR_OUTPUT) $(DEMO_EXEC) $(BAND0_XY) $(DEMO_EDT_MESH) $(DEMO_REF_MESH)
	@mkdir -p $(OUTDIR)/$(MODEL_NAME)
	$(DEMO_EXEC) outdir=$(OUTDIR)/$(MODEL_NAME) ref_mesh=$(DEMO_REF_MESH) edt_mesh=$(DEMO_EDT_MESH) file_allG=$(PREPR_OUTPUT) file_allGradG=$(OUTDIR)/allGradG.mat reg_func=$(REG_FUNC) reg_eps=$(REG_EPS) domain_scale=$(DOMAIN_SCALE) BAND0_XY=$(BAND0_XY) BAND1_XY=$(BAND1_XY) BAND2_XY=$(BAND2_XY) BAND3_XY=$(BAND3_XY) BAND4_XY=$(BAND4_XY) BAND5_XY=$(BAND5_XY) force_scale=$(FORCE_SCALE) 2>&1 | tee $(OUTDIR)/$(MODEL_NAME)/demo_log.txt

$(CONS_OUTPUT): $(PREPR_OUTPUT) $(CONS_EXEC) $(BAND0_XY) $(BAND2_XY) $(DEMO_EDT_MESH) $(DEMO_REF_MESH)
	@mkdir -p $(OUTDIR)/$(MODEL_NAME)
	$(CONS_EXEC) outdir=$(OUTDIR)/$(MODEL_NAME) ref_mesh=$(DEMO_REF_MESH) edt_mesh=$(DEMO_EDT_MESH) file_allG=$(PREPR_OUTPUT) file_allGradG=$(OUTDIR)/allGradG.mat reg_func=$(REG_FUNC) reg_eps=$(REG_EPS) domain_scale=$(DOMAIN_SCALE) BAND0_XY=$(BAND0_XY) BAND1_XY=$(BAND1_XY) BAND2_XY=$(BAND2_XY) BAND3_XY=$(BAND3_XY) BAND4_XY=$(BAND4_XY) BAND5_XY=$(BAND5_XY) 2>&1 | tee $(OUTDIR)/$(MODEL_NAME)/cons_log.txt

$(SPL_OUTPUT): $(PREPR_OUTPUT) $(SPL_EXEC) $(BAND0_XY) $(BAND1_XY) $(DEMO_EDT_MESH) $(DEMO_REF_MESH)
	@mkdir -p $(OUTDIR)/$(MODEL_NAME)
	$(SPL_EXEC) outdir=$(OUTDIR)/$(MODEL_NAME) ref_mesh=$(DEMO_REF_MESH) edt_mesh=$(DEMO_EDT_MESH) file_allG=$(PREPR_OUTPUT) file_allGradG=$(OUTDIR)/allGradG.mat reg_func=$(REG_FUNC) reg_eps=$(REG_EPS) domain_scale=$(DOMAIN_SCALE) BAND0_XY=$(BAND0_XY) BAND1_XY=$(BAND1_XY) BAND2_XY=$(BAND2_XY) BAND3_XY=$(BAND3_XY) BAND4_XY=$(BAND4_XY) BAND5_XY=$(BAND5_XY) force_scale=$(FORCE_SCALE) affine_scale=$(AFFINE_SCALE) suffix=$(SPL_SUFFIX) cx=$(SPL_CX) cy=$(SPL_CY) cz=$(SPL_CZ) F00=$(F00) F01=$(F01) F02=$(F02) F10=$(F10) F11=$(F11) F12=$(F12) F20=$(F20) F21=$(F21) F22=$(F22) L=$(SPL_L) A=$(SPL_A) 2>&1 | tee $(OUTDIR)/$(MODEL_NAME)/spl_log.txt
