#!/bin/bash

num_threads=`echo "$(cat /proc/cpuinfo | grep processor | wc -l)/2" | bc`
echo "threads=${num_threads}"
export OMP_NUM_THREADS=${num_threads}

mtr_name=ABBA
max_e=200
min_e=1

reg_func=gaussian
reg_eps=0.2

for num_band in 10 80; do
num_order=5810

ref_mesh=../data/shirt.obj
edt_mesh=../data/shirt-edit-0.obj
force_scale=250
affine_scale=0

cx=0.0
cy=0.0
cz=0.0

basedir=../result/green/$(date -I)/band-${num_band}/
make -f pipeline.mk spl BASEDIR=${basedir} MTR_NAME=${mtr_name} MAX_E=${max_e} MIN_E=${min_e} NUM_BAND=${num_band} REG_FUNC=${reg_func} REG_EPS=${reg_eps} DEMO_REF_MESH=${ref_mesh} DEMO_EDT_MESH=${edt_mesh} FORCE_SCALE=${force_scale} AFFINE_SCALE=${affine_scale} SPL_SUFFIX=${1} SPL_CX=${cx} SPL_CY=${cy} SPL_CZ=${cz} NUM_ORDER=${num_order}
done
