#!/bin/bash

num_threads=`echo "$(cat /proc/cpuinfo | grep processor | wc -l)/2" | bc`
echo "threads=${num_threads}"
export OMP_NUM_THREADS=${num_threads}

ref_mesh=../data/coarse_cube.obj
edt_mesh=../data/coarse_cube_edit.obj

mtr_name=ORTH
max_e=120
min_e=1

reg_func=gaussian
reg_eps=0.2

num_band=20
num_order=5810

basedir=../result/green/$(date -I)-cube/

make -f pipeline.mk cons BASEDIR=${basedir} MTR_NAME=${mtr_name} MAX_E=${max_e} MIN_E=${min_e} NUM_BAND=${num_band} REG_FUNC=${reg_func} REG_EPS=${reg_eps} DEMO_REF_MESH=${ref_mesh} DEMO_EDT_MESH=${edt_mesh}
