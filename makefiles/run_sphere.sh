#!/bin/bash

num_threads=`echo "$(cat /proc/cpuinfo | grep processor | wc -l)/2" | bc`
echo "threads=${num_threads}"
export OMP_NUM_THREADS=${num_threads}

max_e=100
min_e=1

reg_func=gaussian
reg_eps=0.2

num_band=8
num_order=194

ref_mesh=../data/sphere.obj
edt_mesh=../data/sphere.obj
force_scale=0

cx=0.0
cy=0.0
cz=0.0

f00=-1
f01=0
f02=0
f10=0
f11=0
f12=0
f20=0
f21=0
f22=1

basedir=../result/green/$(date -I)-ANIS/

mtr_name=IMAG
affine_scale=150
img_file=../data/pattern-5.png
make -f pipeline.mk spl BASEDIR=${basedir} MTR_NAME=${mtr_name} MAX_E=${max_e} MIN_E=${min_e} NUM_BAND=${num_band} REG_FUNC=${reg_func} REG_EPS=${reg_eps} DEMO_REF_MESH=${ref_mesh} DEMO_EDT_MESH=${edt_mesh} FORCE_SCALE=${force_scale} AFFINE_SCALE=${affine_scale} SPL_SUFFIX=${1} SPL_CX=${cx} SPL_CY=${cy} SPL_CZ=${cz} NUM_ORDER=${num_order} F00=${f00} F01=${f01} F02=${f02} F10=${f10} F11=${f11} F12=${f12} F20=${f20} F21=${f21} F22=${f22} SPL_L=0 SPL_A=0 MTR_IMG_FILE=${img_file}

mtr_name=HOMO
affine_scale=6000
make -f pipeline.mk spl BASEDIR=${basedir} MTR_NAME=${mtr_name} MAX_E=${max_e} MIN_E=${min_e} NUM_BAND=${num_band} REG_FUNC=${reg_func} REG_EPS=${reg_eps} DEMO_REF_MESH=${ref_mesh} DEMO_EDT_MESH=${edt_mesh} FORCE_SCALE=${force_scale} AFFINE_SCALE=${affine_scale} SPL_SUFFIX=${1} SPL_CX=${cx} SPL_CY=${cy} SPL_CZ=${cz} NUM_ORDER=${num_order} F00=${f00} F01=${f01} F02=${f02} F10=${f10} F11=${f11} F12=${f12} F20=${f20} F21=${f21} F22=${f22} SPL_L=0 SPL_A=0

mtr_name=CIRC
affine_scale=900
make -f pipeline.mk spl BASEDIR=${basedir} MTR_NAME=${mtr_name} MAX_E=${max_e} MIN_E=${min_e} NUM_BAND=${num_band} REG_FUNC=${reg_func} REG_EPS=${reg_eps} DEMO_REF_MESH=${ref_mesh} DEMO_EDT_MESH=${edt_mesh} FORCE_SCALE=${force_scale} AFFINE_SCALE=${affine_scale} SPL_SUFFIX=${1} SPL_CX=${cx} SPL_CY=${cy} SPL_CZ=${cz} NUM_ORDER=${num_order} F00=${f00} F01=${f01} F02=${f02} F10=${f10} F11=${f11} F12=${f12} F20=${f20} F21=${f21} F22=${f22} SPL_L=0 SPL_A=0

mtr_name=SLOP
affine_scale=400
make -f pipeline.mk spl BASEDIR=${basedir} MTR_NAME=${mtr_name} MAX_E=${max_e} MIN_E=${min_e} NUM_BAND=${num_band} REG_FUNC=${reg_func} REG_EPS=${reg_eps} DEMO_REF_MESH=${ref_mesh} DEMO_EDT_MESH=${edt_mesh} FORCE_SCALE=${force_scale} AFFINE_SCALE=${affine_scale} SPL_SUFFIX=${1} SPL_CX=${cx} SPL_CY=${cy} SPL_CZ=${cz} NUM_ORDER=${num_order} F00=${f00} F01=${f01} F02=${f02} F10=${f10} F11=${f11} F12=${f12} F20=${f20} F21=${f21} F22=${f22} SPL_L=0 SPL_A=0
